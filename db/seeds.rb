# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#10.times do |d|
    
Currency.create!(
     currency_name: "USD/RTGS ", 
    )
Currency.create!(
     currency_name: "USD/Bond ", 
    )
Currency.create!(
     currency_name: "Bond/RTGS ", 
    )
Currency.create!(
     currency_name: "OMIR/RTGS ", 
    )
Currency.create!(
     currency_name: "USD/ZAR ", 
    )
Currency.create!(
     currency_name: "ZAR/Bond ", 
    )
Currency.create!(
     currency_name: "ZAR/RTGS ", 
    )