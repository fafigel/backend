class AddPercentageToCurrencies < ActiveRecord::Migration[5.2]
  def change
    add_column :currencies, :percentage, :float
  end
end
