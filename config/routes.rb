Rails.application.routes.draw do
  resources :markets
  resources :currencies
  resources :rates
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
